from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from .serializers import *


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

@api_view(["POST"])
def register(request):

    if request.method == "POST":
        serializer = RegisterUserSerializer(data=request.data)

        data = {}
        if serializer.is_valid(raise_exception=True):

            account = serializer.save()
            data['email'] = serializer.validated_data['email']
            data['username'] = serializer.validated_data['username']
            token = Token.objects.get(user=account).key
            data['token'] = token

        else:
            data = serializer.errors

        return Response(data)

@api_view(['POST'])
def logout(request):

    if request.method == 'POST':
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)

