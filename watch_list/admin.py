from django.contrib import admin
from .models import *


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    pass

@admin.register(StreamPlatform)
class StreamPlatformAdmin(admin.ModelAdmin):
    pass


@admin.register(Review)
class StreamPlatformAdmin(admin.ModelAdmin):
    pass
