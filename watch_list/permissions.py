from rest_framework import permissions


class IsAdminOrReadonly(permissions.IsAdminUser):

    def has_permission(self, request, view):
        admin = bool(request.user and request.user.is_staff)
        return request.method == "GET" or admin

class ReviewUserOrReadonly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return obj.user == request.user


