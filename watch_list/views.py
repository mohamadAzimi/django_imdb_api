from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework.exceptions import ValidationError
from rest_framework import generics
from rest_framework import viewsets

from django.shortcuts import get_object_or_404

from .serializers import *
from .models import *
from .permissions import *


class MovieApiView(generics.ListCreateAPIView):
    """ view to all WatchList in my system """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = [IsAdminOrReadonly]

class MovieDetailApiView(generics.RetrieveUpdateDestroyAPIView):
    """ show specific WatchList instance """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = [IsAdminOrReadonly]


class StreamPlatformViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminOrReadonly]
    queryset = StreamPlatform.objects.all()
    serializer_class = StreamPlatformSerializer


class ReviewList(generics.ListCreateAPIView):
    """ handle review from specific movie """
    def get_queryset(self):
        return Review.objects.filter(movie=self.kwargs.get('pk'))

    serializer_class = ReviewSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        pk = self.kwargs.get('pk')
        user = self.request.user
        movie = Movie.objects.get(pk=pk)
        check_review = Review.objects.filter(user=user, movie=movie)

        if check_review.exists():
            raise ValidationError('you already made review')

        if movie.number_rating == 0:
            movie.avg_rating = serializer.validated_data['rating']
        else:
            movie.avg_rating = (movie.avg_rating + serializer.validated_data['rating'])/2

        movie.number_rating += 1
        movie.save()

        serializer.save(movie=movie, user=user)


class ReviewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [ReviewUserOrReadonly]

