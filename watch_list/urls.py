from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import *


router = DefaultRouter()
router.register('stream', StreamPlatformViewSet, basename='stream-platform')


urlpatterns = [
    path('movie/', MovieApiView.as_view()),
    path('movie/<int:pk>', MovieDetailApiView.as_view()),

    path('', include(router.urls)),

    path('movie/<int:pk>/review/', ReviewList.as_view()),
    path('movie/review/<int:pk>', ReviewDetail.as_view()),

]