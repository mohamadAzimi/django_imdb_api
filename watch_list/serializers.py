from rest_framework import serializers

from .models import *


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Review
        fields = ['id', 'user',  'rating', 'description',  'created_at', 'updated_at']


class MovieSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = ['id',
                  'title',
                  'description',
                  'is_active',
                  'platform',
                  'reviews', 'avg_rating', 'number_rating']


class StreamPlatformSerializer(serializers.ModelSerializer):
    movies = MovieSerializer(many=True, read_only=True)

    class Meta:
        model = StreamPlatform
        fields = ['id', 'name', 'about', 'website', 'movies']



